#![no_main]
#![no_std]

use cortex_m::asm;
use cortex_m_rt::entry;
use embedded_hal::digital::v2::{InputPin, OutputPin};
use keyberon::key_code::{KbHidReport, KeyCode};
use rtt_target::{rprintln, rtt_init_print};
use stm32f1xx_hal::usb::{Peripheral, UsbBus, UsbBusType};
use stm32f1xx_hal::{adc, prelude::*};
use usb_device::bus::UsbBusAllocator;
use usb_device::class::UsbClass as _;

#[panic_handler]
fn panic(info: &core::panic::PanicInfo) -> ! {
    rprintln!("{}", info);
    exit()
}

fn exit() -> ! {
    loop {
        asm::bkpt() // halt = exit probe-run
    }
}

// no idea if this structure is useful for anything,
// but at least it documents the current connections
/*
pub struct Pedals(
    // Pedal A
    PA1<Input<PullDown>>, // NC
    PA2<Output<PushPull>>, // COM
    PA3<Input<PullDown>>, // NO

    // Pedal B
    PA4<Input<PullDown>>, // NC
    PA5<Output<PushPull>>, // COM
    PA6<Input<PullDown>> // NO
);
*/

type UsbClass = keyberon::Class<'static, UsbBusType, ()>;
type UsbDevice = usb_device::device::UsbDevice<'static, UsbBusType>;

#[entry]
fn main() -> ! {
    rtt_init_print!();
    rprintln!("started main() and enabled RTT printing");

    //let core = cortex_m::Peripherals::take().unwrap();
    let device = stm32f1xx_hal::stm32::Peripherals::take().unwrap();
    let mut rcc = device.RCC.constrain();
    let mut flash = device.FLASH.constrain();

    let clocks = rcc
        .cfgr
        .use_hse(8.mhz())
        .sysclk(48.mhz())
        .pclk1(24.mhz())
        .freeze(&mut flash.acr);

    assert!(clocks.usbclk_valid());

    // configure the user led
    let mut gpioc = device.GPIOC.split(&mut rcc.apb2);
    let mut led = gpioc.pc13.into_push_pull_output(&mut gpioc.crh);
    led.set_high().unwrap(); // active low

    // and the pins used by the pedals
    // pedal A, SPDT with NC, COM, NO)
    let mut gpioa = device.GPIOA.split(&mut rcc.apb2);
    let a_nc = gpioa.pa1.into_pull_down_input(&mut gpioa.crl);
    let mut a_com = gpioa.pa2.into_push_pull_output(&mut gpioa.crl);
    let a_no = gpioa.pa3.into_pull_down_input(&mut gpioa.crl);
    a_com.set_high().unwrap();

    // pedal B
    let b_nc = gpioa.pa4.into_pull_down_input(&mut gpioa.crl);
    let mut b_com = gpioa.pa5.into_push_pull_output(&mut gpioa.crl);
    let b_no = gpioa.pa6.into_pull_down_input(&mut gpioa.crl);
    b_com.set_high().unwrap();

    rprintln!("Set up the pedals' pins");

    // ADC for internal temperature
    let mut adc = adc::Adc::adc1(device.ADC1, &mut rcc.apb2, clocks);

    // USB setup
    rprintln!("setting up the USB setup");


    let usb_dm = gpioa.pa11;
    let usb_dp = gpioa.pa12;

/*
    // reset USB
    // may only be required when R10 (DP pulldown is 10k (marking 103))
    // and only needed if you want to avoid replugging it after flashing
    // (my unit has had R10 replaced with the correct 1.5k (marking 152))
    // remove above "let usb_dp" if you need this block
    // TODO: figure out feature flags
    let mut usb_dp = gpioa.pa12.into_push_pull_output(&mut gpioa.crh);
    usb_dp.set_low().unwrap();
    asm::delay(clocks.sysclk().0 / 100);
    let usb_dp = usb_dp.into_floating_input(&mut gpioa.crh);
*/

    rprintln!("USB pins set up");

    let usb = Peripheral {
        usb: device.USB,
        pin_dm: usb_dm,
        pin_dp: usb_dp,
    };

    let usb_bus = cortex_m::singleton!(: UsbBusAllocator<UsbBusType> = UsbBus::new(usb)).unwrap();

    let mut usb_class = keyberon::new_class(usb_bus, ());
    let mut usb_dev = keyberon::new_device(usb_bus);

    rprintln!("USB setup set up");

    // end of "context setup"

    // persist pedal states across loops
    let mut keycodes = [KeyCode::No, KeyCode::No];
    let mut old_keycodes = keycodes;

    rprintln!("internal sensor: {}°C", adc.read_temp());

    loop {

        // handle pedal A
        if a_no.is_high().unwrap() {
            keycodes[0] = KeyCode::ScrollLock;
            rprintln!("pedal A pressed");
        }
        else if a_nc.is_high().unwrap() {
            keycodes[0] = KeyCode::No;
            rprintln!("pedal A released");
        }
        else if a_no.is_low().unwrap() && a_nc.is_low().unwrap() {
            rprintln!("pedal A bounced");
        };

        // handle pedal B
        if b_no.is_high().unwrap() {
            keycodes[1] = KeyCode::MediaPlayPause;
            rprintln!("pedal B pressed");
        }
        else if b_nc.is_high().unwrap() {
            keycodes[1] = KeyCode::No;
            rprintln!("pedal B released");
        }
        else if b_no.is_low().unwrap() && b_nc.is_low().unwrap() {
            rprintln!("pedal B bounced");
        };


        // light the LED if either is pressed
        if keycodes[0] != KeyCode::No || keycodes[1] != KeyCode::No {
            led.set_low().unwrap();
        }
        else {
            led.set_high().unwrap();
        }


        usb_poll(&mut usb_dev, &mut usb_class);
        if keycodes != old_keycodes {
            send_report(
                core::array::IntoIter::new(keycodes),
                &mut usb_dev,
                &mut usb_class,
            );
            old_keycodes = keycodes;
        };
    }
}

fn usb_poll(usb_dev: &mut UsbDevice, keyboard: &mut UsbClass) {
    if usb_dev.poll(&mut [keyboard]) {
        keyboard.poll();
    }
}

fn send_report(
    iter: impl Iterator<Item = KeyCode>,
    usb_dev: &mut UsbDevice,
    usb_class: &mut UsbClass,
) {
    let report: KbHidReport = iter.collect();
    if usb_class.device_mut().set_keyboard_report(report.clone()) {
        while let Ok(0) = usb_class.write(report.as_bytes()) {
            usb_poll(usb_dev, usb_class);
        }
    }
}
