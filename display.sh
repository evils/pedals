#!/usr/bin/env nix-shell
#!nix-shell --pure -i bash -p toilet

show() {
  measurement="$(awk '{ print $2" "$3 }' measurement)"
  toilet -F border -f mono12.tlf -t -- "${measurement}" 1>&2
  printf "\033[12A" 1>&2
}

while true; do
  show
  sleep 0.1
done
