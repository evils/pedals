/*
Linker script for the STM32F103C8T6
FLASH may be offset if the USB DFU bootloader occupies 0x8000000
*/
MEMORY
{
  FLASH : ORIGIN = 0x08001000, LENGTH = 64K
  RAM : ORIGIN = 0x20000000, LENGTH = 20K
}
