#!/usr/bin/env nix-shell
#!nix-shell --pure -i bash -p mosquitto

host="mqtt"
topic="temperature"

now() {
  date +%s.%3N
}

runner() {
  echo "starting mqtt runner"
  mosquitto_sub -h "${host}" -t "${topic}" |
  while read -r line; do
    echo "${line}" " $(now)" >  .mqtt-measurement
    mv .mqtt-measurement mqtt-measurement
  done
  echo "past loop"
}

runner
