.PHONY: all
all: *.adoc
	asciidoctor *.adoc

.PHONY: clean
clean:
	rm *.html

.PHONY: check
check:
	shellcheck --shell=bash *.sh -o all
