= USB foot pedals
Evils <evils.devils@protonmail.com>
:homepage: https://gitlab.com/evils/proto_amp
:license: ISC
:keywords: Rust, STM32F103, blue, pill, foot, pedal, pedals, keyboard, keyberon, nix, probe, run, st, link, cargo, embed, usb, dfu, firmware, bootloader
:toc2:
:sectanchors:
:sectlinks:
:hardbreaks-option:

An attempt at keyboard firmware for some foot pedals on a blue pill.
With the <<purpose.sh, purpose>> of triggering the execution of a script on linux.
And an extra pedal for media play/pause while i'm at it.

== Toolchain
The toolchain for this project is provided via Nix.
This requires `nix` to be installed (https://nixos.org/download.html#nix-quick-install[see here]).
To allow access you may want the appropriate udev rules set up for whatever probe/link you're using,
though running the nix provided tools as root should work.

== Flashing
=== Boot pins
the STM32F103 has two boot selection pins, `BOOT0` and `BOOT1`
their setting takes effect when the chip is reset
since SWD should always work, only `BOOT0 == 0 && BOOT1 == 0` is used here
that mode selects main flash as the boot space (user code)
`BOOT0 == 1 && BOOT1 == 1` selects SRAM for booting (volatile)
`BOOT0 == 1 && BOOT1 == 0` selects the USART bootloader (non-volatile, read-only)

=== memory.x
the "linker script" in memory.x holds 2 magic numbers
one indicating the starting point (in the address space) of the RAM
one indicating the starting point of the flash region, where execution begins

when using the USB DFU bootloader, _it_ occupies the actual starting point (`0x08000000`)
when loading firmware via DFU,
that bootloader will put the firmware in flash starting at `0x08001000` and start execution there
therefore, when flashing firmware via another method while keeping the DFU bootloader,
the starting point must be set to `0x08001000` to avoid overwriting the DFU bootloader and have it execute the firmware

=== Via SWD (st-link etc)
connect the blue pill via an SWD adapter (st-link, j-link, etc)

==== probe-run
`cargo run --release`

==== cargo embed
`cargo embed --release`
this is equivalent to the `cargo run` option, but could leave that command open for running on the computer?
the setup for this is maintained in `Embed.toml`, which is a bit less obscure than being hidden in `.cargo/config`
that also is a bit more verbose with more discoverable options?

==== Via OpenOCD + GDB
build the latest firmware
(optionally run `cargo clean` first)
`cargo build --release`
start OpenOCD (this facilitates communication with the chip via the probe (st-link, etc) and uses `openocd.cfg`)
`openocd`
start GDB and execute the commands found in `openocd.gdb` with the additional argument of the binary's path
(that path is given to the last command in `openocd.gdb` which is `load` which loads the binary onto the chip)
`gdb -x openocd.gdb target/thumbv7m-none-eabi/release/pedals`
this leaves you in a gdb session

==== Bootloader
`st-flash write bootloader-dfu-fw.bin 0x08000000`
plenty of other ways to do this, but this is straight-forward and works for me

this bootloader occupies some space on the flash
therefore the origin point in `memory.x` is changed to `0x08001000`
and `--nmagic` passed to the linker (via `.cargo/config`) to allow for not being page-aligned
this means once the bootloader is flashed, the other flashing methods don't replace it
to overwrite this bootloader, first change the flash origin in `memory.x` to `0x08000000` and `cargo clean`

[sidebar]
the bootloader binary provided was created from https://github.com/davidgfnet/stm32-dfu-bootloader/tree/ad01a0b0e61ef91bfaf423081143d3e963f52315[here]
with `gcc-arm-embedded-9` from nixpkgs
without `ENABLE_PROTECTIONS ENABLE_WATCHDOG` and with `ENABLE_DFU_UPLOAD ENABLE_PINRST_DFU_BOOT`

=== Via bootloader
[sidebar]
this requires <<Bootloader,the DFU bootloader>> to be on the device

[sidebar]
set a udev rule like this to allow using this as a regular user
```
# davidgfnet stm32-dfu-bootloader
ATTRS{idVendor}=="dead", ATTRS{idProduct}=="ca5d", MODE:="0666"
```

connect to the computer via a data capable USB cable
press the reset button, this should make the bootloader enter DFU mode
`cargo make dfu`
[sidebar]
this performs the equivalent of the following
`cargo objcopy --bin pedals --release -- -O binary target/thumbv7m-none-eabi/release/pedals.bin`
`dfu-util -d dead:ca5d -D target/thumbv7m-none-eabi/release/pedals.bin --dfuse-address 0x08001000`
(which is defined in Makefile.toml)

== purpose.sh
a script that outputs CSV values from my multimeter on `STDOUT` when pedal A is pressed
it'll beep when the measurement fails, and not provide a bad measurement

[sidebar]
while the dependencies of the script are provided by the script via `nix-shell`,
the script depends on having access to the brymen USB interface and the `pcspkr`
in my case, these permissions are set up via udev

[sidebar]
`runner.sh` and `display.sh` were split off from this, and should be integrated back in
hence their functions are still included in this script

=== runner.sh
continuously saves the latest measurement in a file named `measurement`
this needs to be running for `purpose.sh` to work

=== display.sh
shows the latest `measurement` from `runner.sh` in some nice big readable ASCII art

== Credits
started with
https://github.com/TeXitoi/blue-pill-quickstart
with a bootloader from
https://github.com/davidgfnet/stm32-dfu-bootloader
