{pkgs ? import <nixpkgs> {
  overlays = [
    (import (builtins.fetchTarball https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz))
  ];
  }
}:

with pkgs;

stdenv.mkDerivation {
  name = "rustenv";

  nativeBuildInputs = [
    # nix native alternative to rustup to get the toolchain
    # also provides cargo, rustfmt, etc
    (pkgs.rustChannelOfTargets "stable" null [ "thumbv7m-none-eabi" ])
    # for uploading
    probe-run
    cargo-embed
    stlink # for the DFU bootloader
    # for dfu-util (downloading)
    pkg-config libusb1 dfu-util
    cargo-make
    cargo-binutils
    # https://github.com/mozilla/nixpkgs-mozilla/issues/223
    # suggests binutils needs gcc-arm-embedded, dfu download works without it though
    #gcc-arm-embedded
    # for cargo-make
    openssl
    # for debugging
    gdb
    openocd
    evtest

    # for building README.html
    asciidoctor
  ];

  RUST_BACKTRACE = 1;
  # hint, use lorri shell if you want to keep your shell and prompt
}
