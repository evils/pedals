#!/usr/bin/env nix-shell
#!nix-shell --pure -i bash -p sigrok-cli

now() {
  date +%s.%3N
}

runner() {
  echo "starting dmm runner"
  sigrok-cli --driver brymen-bm86x --continuous --channels "P1" |
  while read -r line; do
    awk '/P1/ {print $2 " " $3}' <<< "${line}" | xargs -0 printf "%s %s" "$(now)" > .measurement
    mv .measurement measurement
  done
  echo "past loop"
}

runner
