#!/usr/bin/env nix-shell
#!nix-shell --pure -i bash -p evtest sigrok-cli beep bc toilet

now() {
  date +%s.%3N
}

get() {
  timeout 1 sigrok-cli --driver brymen-bm86x --samples 1
}

runner() {
  echo "starting runner"
  sigrok-cli --driver brymen-bm86x --continuous --channels "P1" |
  while read -r line; do
    echo "top of loop"
    awk '/P1/ {print $2 " " $3}' <<< "${line}" | xargs -0 printf "%s %s" "$(now)" > .measurement
    mv .measurement measurement
  done
  echo "past loop"
}

ms_diff() {
    s_1="$(cut -d "." -f 1 <<< "$1")"
    m_1="$(cut -d "." -f 2 <<< "$1")"

    s_2="$(cut -d "." -f 1 <<< "$2")"
    m_2="$(cut -d "." -f 2 <<< "$2")"

    if ((s_1 > s_2)); then
      echo "( ( 1000 * (${s_1} - ${s_2}) ) + (${m_1} - ${m_2}) )" | bc
    else
      s="$( echo "( 1000 * (${s_2} - ${s_1}) )" | bc)"
      if (( 10#${m_1} > 10#${m_2} )); then
        echo "( ${s} + (${m_1} - ${m_2}) )" | bc
      else
        echo "( ${s} + (${m_2} - ${m_1}) )" | bc
      fi
    fi
}

lapsed() {
  diff="$(ms_diff "$(now)" "$1")"
  if (( diff > $2 )); then
    beep -f 1024 -l 256
    true
  else
    false
  fi
}

count=0

# assumes udev setup to allow access to the brymen usb interface (0820:0001) and pcspkr
payload() {
  if ! lapsed "$(cut -d " " -f 1 measurement)" "256"; then
    time="$(awk '{ print $1 }' measurement)"
    measurement="$(awk '{ print $2","$3 }' measurement)"
    temperature="$(awk '{ print $1 ",°C" }' mqtt-measurement)"
    if [[ ${measurement} != "" ]]; then
      printf "%d,%s,%s,%s\n" "${count}" "${time}" "${measurement}" "${temperature}"
      printf "%d %s %s %s\n" "${count}" "${time}" "${measurement}" "${temperature}" 1>&2
      (( count++ ))
    else
      beep -f 1024 -l 256
    fi
  fi
}

# using this results in some troubling performance (missing pedal events / catching up later)
show() {
  measurement="$(awk '{ print $2" "$3 }' measurement)"
  toilet -F border -f mono12.tlf -- "${measurement}" 1>&2
  printf "\033[12A" 1>&2
}

printf "Start of run at %s\n" "$(now)"
printf "Start of run at %s\n" "$(now)" 1>&2
evtest --grab /dev/input/by-id/usb-RIIR_Task_Force_Keyberon_0.1.1-event-kbd |
while read -r line; do
  # value 1 means pressed down (value 0 is release, value 2 is repeat)
  if echo "${line}" | grep -q "KEY_SCROLLLOCK), value 1"; then
    payload
  fi
done
